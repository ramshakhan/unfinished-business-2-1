import java.util.*;

public class Population {

    private ArrayList<Individual> individuals = new ArrayList<>();
    private ArrayList<Individual> newIndividuals = new ArrayList<>();
    private final int POPULATION_SIZE = 300;
    private final int INDIVIDUAL_SIZE = 100;
    private AlgorithmSelector algorithmSelector;

    public Population(ArrayList<Individual> individuals, AlgorithmSelector algorithmSelector) {
        this.individuals = individuals;
        this.algorithmSelector = algorithmSelector;
    }

//    // Print out every Individual in the population (debugging purposes only)
//    public void printPopulation() {
//        for (int i = 0; i < POPULATION_SIZE; i++) {
//            for (int j = 0; j < INDIVIDUAL_SIZE; j++) {
//                System.out.print(individuals.get(i).getChromosome()[j] + "\t\t\t\t\t    ");
//            }
//            System.out.println();
//        }
//    }

    // Print out the Maximum Fitness of the population
    public double maxPopulation() {
        double max = individuals.get(0).individualEvaluation();
        for (int i = 1; i < individuals.size(); i++) {
            if (individuals.get(i).individualEvaluation() > max) {
                max = individuals.get(i).individualEvaluation();
            }
        }
        return max;
    }

    // Print out the Minimum Fitness of the population
    public double minPopulation() {
        double min = individuals.get(0).individualEvaluation();
        for (int i = 0; i < individuals.size(); i++) {
            if (individuals.get(i).individualEvaluation() < min) {
                min = individuals.get(i).individualEvaluation();
            }
        }
        return min;
    }

    // Print out the Average Fitness of the population
    public double averagePopulation() {
        double average = 0.0;
        for (int i = 0; i < individuals.size(); i++) {
            average = average + individuals.get(i).individualEvaluation();
        }
        average = average / individuals.size();
        return average;
    }

    // Tournament Selection Process, Select 2 best parents and then move on to Crossover Process
    public void tournamentSelectionProcess(int sizeOfSelection) {
        while (newIndividuals.size() <= POPULATION_SIZE) {
            // Select Parent for Tournament Selection
            Individual parent1 = selectParent(sizeOfSelection);
            Individual parent2 = selectParent(sizeOfSelection);
            crossoverProcess(parent1, parent2);
        }
    }

    // Clears the Individuals Array and add in the New Individuals values in it, then clear the New Individuals Array
    public void moveNewPopulationToOldPopulation() {
        individuals.clear();
        individuals.addAll(newIndividuals);
        newIndividuals.clear();
    }

    // Select Parent for Tournament Selection Process
    // Returns a parent based on its minimum fitness after selecting a bunch of Individuals based on size
    public Individual selectParent(int size) {
        Random random = new Random();

        // Add initial Parents from Random Individual
        int chromosome_index = random.nextInt(POPULATION_SIZE);
        Individual parent = individuals.get(chromosome_index);

        for (int i = 0; i < size; i++) {
            if (individuals.get(chromosome_index).individualEvaluation() <= parent.individualEvaluation()){
                parent = individuals.get(chromosome_index);
            }
            chromosome_index = random.nextInt(POPULATION_SIZE);
        }
        return parent;
    }

    // Single Point Crossover (Random locus point)
    public void crossoverProcess(Individual parent1, Individual parent2) {
        Random random = new Random();

        // First Child
        double child1[] = new double[INDIVIDUAL_SIZE];
        double chance = random.nextDouble();
        int locus = random.nextInt(INDIVIDUAL_SIZE);
        if (chance <= 0.7) { // 70% Chance to Crossover
            for (int i = 0; i <= locus; i++) {
                child1[i] = parent1.getChromosome()[i];
            }
            for (int i = locus; i < INDIVIDUAL_SIZE; i++) {
                child1[i] = parent2.getChromosome()[i];
            }
            newIndividuals.add(new Individual(child1, algorithmSelector.getSelector()));
        } else { // 30% Chance to not Crossover
            newIndividuals.add(parent1);
        }

        // Second Child
        double child2[] = new double[INDIVIDUAL_SIZE];
        chance = random.nextDouble();
        locus = random.nextInt(INDIVIDUAL_SIZE);
        if (chance <= 0.7) { // 70% Chance to Crossover
            for (int i = 0; i <= locus; i++) {
                child2[i] = parent2.getChromosome()[i];
            }
            for (int i = locus; i < INDIVIDUAL_SIZE; i++) {
                child2[i] = parent1.getChromosome()[i];
            }
            newIndividuals.add(new Individual(child2, algorithmSelector.getSelector()));
        } else { // 30% Chance to not Crossover
            newIndividuals.add(parent2);
        }
    }

    // Mutation Process, Mutate one Alleles by modifying the values between 0.5 and -0.5 from one Individual only
    // Iterate through every Individuals with 1% mutation,
    // and if that individuals is selected for mutation, one of its alleles will be change and move on.
    // TODO: mutation chance for all genes of each individuals
    public void mutationProcess() {
        double maxIndividual = algorithmSelector.getMax();
        double minIndividual = algorithmSelector.getMin();
        double max = maxIndividual * 0.09765625;
        double min = minIndividual * 0.09765625;
        Random random = new Random();

        for (int i = 0; i < newIndividuals.size(); i++) {
            if (random.nextDouble() <= 0.01) { // 1% Chance to Mutate
                int mutationChance = random.nextInt(INDIVIDUAL_SIZE);
                double temp = newIndividuals.get(i).getChromosome()[mutationChance];
                temp += (random.nextDouble() * (max - min)) + min;

                // Check whether the alleles will exceed the boundary
                while (temp > maxIndividual || temp < minIndividual) {
                    temp = newIndividuals.get(i).getChromosome()[mutationChance];
                    temp += (random.nextDouble() * (max - min)) + min;
                }
                newIndividuals.get(i).getChromosome()[mutationChance] = temp;
            }
        }
    }
}